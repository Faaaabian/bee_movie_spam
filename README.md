# Bee Movie Spam and Keylogger

## Bee Movie Spam

Basically it does what it says - after the Bee Movie spam script is startet, you have a countdown of 10 seconds, before it starts to type every single line of the "Bee movie" - and your computer thinks it's you! This goes on until either the script ends (in line 4566) or you press "esc". I came up with this little toy because it's fun and it was on reddit xD

## Keylogger

Also intuitive is the Keylogger. This one was really just because I had fun building one and had nothing better to do (during coronacation). You start the script and it will log every single keypress until you press exit. It is also possible to run it in the background.